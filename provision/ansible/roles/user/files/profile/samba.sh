#!/usr/bin/env bash

# Verifie que le service docker est lancé.
# Si ce n'est pas le cas alors le démarre
# Lance le container samba pour developpé s'il existe sinon le créé
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
SHARE_PATH="${1:-/home/vagrant/projects}"

SMB_NAME=samba
USERID=1000
GROUPID=1000

CONTAINER=$(docker ps -qa -f name=${SMB_NAME})
CONTAINER_NUMBER=$(docker ps -qa -f name=${SMB_NAME}|wc -l)

if [ "sudo systemctl -q status docker" ]; then
  sudo systemctl start docker
fi

if [ "${CONTAINER_NUMBER}" -eq 0 ]; then 
  docker run --net=host --name ${SMB_NAME} \
    -v ${SHARE_PATH}:/share \
    -e USERID=${USERID} -e GROUPID=${GROUPID} \
    -d dperson/samba \
    -u "smbuser;mdp;${USERID};${GROUPID}" \
    -s "projects;/share;yes;no;yes;all"
elif [ "docker ps -qa -f name=${SMB_NAME} -f exited=0" ]; then
  docker start ${SMB_NAME}
fi
