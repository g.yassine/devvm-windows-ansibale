# Get latest container ID
alias dl="docker ps -l -q"

# Show logs tail of container name
alias dlogs='docker logs -f --tail 33 $1'

# Get container process
alias dps="docker ps"

# Get process included stop container
alias dpa="docker ps -a"

# Get images
alias di="docker images"

# Get container IP
alias dip="docker inspect --format '{{ .NetworkSettings.IPAddress }}'"

# Run deamonized container, e.g., $dkd base /bin/echo hello
alias dkd="docker run -d -P"

# Run interactive container, e.g., $dki base /bin/bash
alias dki="docker run -i -t -P"

# Execute interactive container, e.g., $dex base /bin/bash
alias dex="docker exec -i -t"

# Show mounts of one container, e.g., $dmounts $(docker ps -q |tail -n 1)
dmounts() { docker inspect --format='{{json .Mounts}}' $1 | jq; }

# Show stats with the names of the containers
dstats() { docker stats $(docker ps --format={{.Names}}); }

# Stop all containers
dstop() { docker stop $(docker ps -a -q); }

# Remove all containers
drm() { docker rm $(docker ps -a -q); }

# Stop and Remove all containers
alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'

# Remove all images
dri() { docker rmi $(docker images -q); }

# Dockerfile build, e.g., $dbu tcnksm/test
dbu() { docker build -rm -t=$1 .; }

# Show all alias related docker
dalias() { alias | grep 'docker' | sed "s/^\([^=]*\)=\(.*\)/\1 => \2/"| sed "s/['|\']//g" | sort; }

# Clean-up useless containers and images
alias dclean='docker volume ls -qf "dangling=true" | xargs --no-run-if-empty docker volume rm && docker images -qf "dangling=true" | xargs --no-run-if-empty docker rmi'

# sass
alias sass="docker run -it --rm -v \$(pwd):\$(pwd) -w \$(pwd) jbergknoff/sass"

# node
alias node="docker run -it --rm -v \$PWD:/usr/src/app -w /usr/src/app node:alpine node"
alias npm="docker run -it --rm -v \$PWD:/usr/src/app -w /usr/src/app node:alpine npm"
alias yarn="docker run -it --rm -v \$PWD:/usr/src/app -w /usr/src/app node:alpine yarn"
alias composer="docker run -ti --rm -v \$(pwd):/app -u \$UID:\$GID -w /app registry.intranet.ageas.fr:5000/php-apache:latest php -d memory_limit=-1 /usr/local/bin/composer"
alias php="docker run -ti --rm -v \$(pwd):/app -w /app -u application --link oracle_db  --net docker_ariane_default --link rabbit registry.intranet.ageas.fr:5000/php-apache:latest php -d memory_limit=-1"
alias behat='php /app/bin/behat'
alias phpunit="docker run -ti --rm -v \$(pwd):/app -u application -w /app --add-host sicav-dev2:192.168.10.31 --net docker_ariane_default --link rabbit --link mailcatcher --link phantomjs registry.intranet.ageas.fr:5000/php-apache:latest php -d memory_limit=-1 /app/bin/phpunit"

# Launch "service" samba
alias dsamba="CONT_SAMBA=samba;docker ps -qf name=${CONT_SAMBA}||docker ps -aqf name=${CONT_SAMBA} && docker start ${CONT_SAMBA}||docker run --net=host --name ${CONT_SAMBA} -v /home/vagrant/projects:/share -e USERID=1000 -e GROUPID=1000 -d dperson/samba -u 'smbuser;mdp;1000;1000' -s 'projects;/share;yes;no;yes;all'"
