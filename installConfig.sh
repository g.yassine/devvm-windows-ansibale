#! /bin/bash

cp /vagrant/id_rsa_deploy* /vagrant/config.user ~/.ssh
chmod 0600 ~/.ssh/id_rsa_deploy*

echo "Copie des fichiers de config et des clés de déploiement SSH effectuée !"